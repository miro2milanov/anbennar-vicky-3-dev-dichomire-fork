﻿force_state_religion = {
	groups= {
		power_bloc_leader
	}
	show_in_lens = no

	texture = "gfx/interface/icons/diplomatic_action_icons/force_state_religion.dds"
	
	selectable = {
		is_power_bloc_leader = yes
		power_bloc ?= {
			modifier:power_bloc_leader_can_force_state_religion_bool = yes
		}		
	}
	
	potential = {
		power_bloc ?= {
			scope:target_country.power_bloc ?= this
		}
	}
	
	possible = {
		scope:target_country = {
			NOT = { religion = root.religion }
		}

		power_bloc = {
			current_cohesion_percentage >= 0.5
		}
		scope:target_country = {
			tenure_in_current_power_bloc_years >= 5
		}
		prestige >= {
			value = scope:target_country.prestige
			multiply = 3
		}
	}
	
	accept_effect = {
		scope:target_country = {
			set_state_religion = root.religion
			capital = {
				convert_population = {
					target = root.religion
					value = 0.05
				}
			}
			every_scope_state = {
				limit = { is_capital = no }
				convert_population = {
					target = root.religion
					value = 0.02
				}				
			}
			state_religion_switch_effect = yes
		}
		
		power_bloc = {
			add_cohesion_percent = scaled_cohesion_cost
		}
	}
	
	ai = {
		evaluation_chance = {
			value = 0.025 # AI shouldn't use this very often
		}
		
		will_propose = {
			power_bloc = { current_cohesion_percentage >= 0.75 }
			OR = {
				has_attitude = {
					who = scope:target_country
					attitude = domineering
				}
			#	AND = {
			#		religion = { has_discrimination_trait = christian }
			#		NOT = { scope:target_country = { religion = { has_discrimination_trait = christian } } }
			#	}
			#	AND = {
			#		religion = { has_discrimination_trait = muslim }
			#		NOT = { scope:target_country = { religion = { has_discrimination_trait = muslim } } }
			#	}
			#	AND = {
			#		religion = { has_discrimination_trait = jewish }
			#		NOT = { scope:target_country = { religion = { has_discrimination_trait = jewish } } }
			#	}
			#	AND = {
			#		religion = { has_discrimination_trait = eastern }
			#		NOT = { scope:target_country = { religion = { has_discrimination_trait = eastern } } }
			#	}
			#	AND = {
			#		religion = { has_discrimination_trait = buddhist }
			#		NOT = { scope:target_country = { religion = { has_discrimination_trait = buddhist } } }
			#	}				
			#	AND = {
			#		religion = { has_discrimination_trait = animist }
			#		NOT = { scope:target_country = { religion = { has_discrimination_trait = animist } } }
			#	}	
				AND = {
					religion = { has_discrimination_trait = sun_cult }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = sun_cult } } }
				}	
				AND = {
					religion = { has_discrimination_trait = cannorian_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = cannorian_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = raheni_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = raheni_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = dwarven_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = dwarven_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = gerudian_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = gerudian_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = orcish_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = orcish_religion } } }
				}	
				AND = {
					religion = { has_discrimination_trait = soruinic_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = soruinic_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = dragon_cult }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = dragon_cult } } }
				}
				AND = {
					religion = { has_discrimination_trait = ynnic_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = ynnic_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = goblin_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = goblin_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = fey }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = fey } } }
				}
				AND = {
					religion = { has_discrimination_trait = taychendi_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = taychendi_religion } } }
				}	
				AND = {
					religion = { has_discrimination_trait = eordan_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = eordan_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = rzentur_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = rzentur_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = halessi_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = halessi_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = giantkin }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = giantkin } } }
				}
				AND = {
					religion = { has_discrimination_trait = centaur_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = centaur_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = triunic_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = triunic_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = elven_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = elven_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = harpy_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = harpy_religion } } }
				}	
				AND = {
					religion = { has_discrimination_trait = effelai_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = effelai_religion } } }
				}				
				AND = {
					religion = { has_discrimination_trait = harafic_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = harafic_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = noruinic_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = noruinic_religion } } }
				}
				AND = {
					religion = { has_discrimination_trait = eldritch_cult }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = eldritch_cult } } }
				}
				AND = {
					religion = { has_discrimination_trait = psychic_hivemind }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = psychic_hivemind } } }
				}
				AND = {
					religion = { has_discrimination_trait = bharbhen_religion }
					NOT = { scope:target_country = { religion = { has_discrimination_trait = bharbhen_religion } } }
				}			
			}
		}

		propose_score = {								
			value = 10			
		}
	}
}
