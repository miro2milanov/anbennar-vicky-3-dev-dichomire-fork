﻿#Likes free artifice, dislikes magic, generally ok with some evilness
ideology_artifice_supremacist = {
	
	lawgroup_magic_and_artifice = {
		law_artifice_banned = strongly_disapprove
		law_nation_of_magic = neutral
		law_nation_of_green_artifice = approve
		law_nation_of_artifice = approve
		law_traditional_magic_banned = strongly_approve
	}
}

#Likes Magic
ideology_magical_supremacist = {
	lawgroup_magic_and_artifice = {
		law_artifice_banned = strongly_approve
		law_nation_of_magic = approve
		law_nation_of_green_artifice = approve
		law_nation_of_artifice = disapprove
		law_traditional_magic_banned = strongly_disapprove
	}
}

##unused
# ideology_equal = {
# 	icon = "gfx/interface/icons/ideology_icons/isolationist.dds"
	
# 	lawgroup_rights_of_women = {
# 		law_womens_suffrage = neutral
# 		law_women_in_the_workplace = neutral
# 		law_women_own_property = neutral
# 		law_no_womens_rights = disapprove
# 	}

# 	lawgroup_free_speech = {
# 		law_outlawed_dissent = approve
# 		law_censorship = approve
# 		law_right_of_assembly = neutral
# 		law_protected_speech = disapprove
# 	}
# }

ideology_matriarchal = {
	icon = "gfx/interface/icons/ideology_icons/patriarchal.dds"
	
	lawgroup_rights_of_women = {
		law_no_womens_rights = approve
		law_women_own_property = disapprove
		law_women_in_the_workplace = disapprove
		law_womens_suffrage = strongly_disapprove
	}

	lawgroup_free_speech = {
		law_outlawed_dissent = approve
		law_censorship = approve
		law_right_of_assembly = neutral
		law_protected_speech = disapprove
	}
}


ideology_machine_servitude = {
	icon = "gfx/interface/icons/ideology_icons/plutocratic.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = strongly_disapprove
		law_mechanim_banned = strongly_disapprove
		law_sapience_mechanim_compromise_enforced = approve
		law_sapience_mechanim_compromise = approve
		law_sapience_mechanim_unrecognized_enforced = strongly_approve
		law_sapience_mechanim_unrecognized = strongly_approve
	}
}

ideology_luddite_mechanim = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/luddite.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = strongly_disapprove
		law_mechanim_banned = approve
		law_sapience_mechanim_compromise_enforced = neutral
		law_sapience_mechanim_compromise = neutral
		law_sapience_mechanim_unrecognized_enforced = disapprove
		law_sapience_mechanim_unrecognized = disapprove
	}

}

ideology_anti_mechanim_trade = {
	icon = "gfx/interface/icons/ideology_icons/abolitionist.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = approve
		law_mechanim_banned = neutral
		law_sapience_mechanim_compromise_enforced = disapprove
		law_sapience_mechanim_compromise = disapprove
		law_sapience_mechanim_unrecognized_enforced = strongly_disapprove
		law_sapience_mechanim_unrecognized = strongly_disapprove
	}
}

ideology_pb_machine_servitude = {
	icon = "gfx/interface/icons/ideology_icons/plutocratic.dds"
	lawgroup_mechanim_rights = {
		law_mechanim_freedom = neutral
		law_mechanim_banned = disapprove
		law_sapience_mechanim_compromise_enforced = approve
		law_sapience_mechanim_compromise = approve
		law_sapience_mechanim_unrecognized_enforced = neutral
		law_sapience_mechanim_unrecognized = neutral
	}
}

ideology_archatiradist = {
	icon = "gfx/interface/icons/ideology_icons/reactionary.dds"
	lawgroup_governance_principles = {
		law_presidential_republic = strongly_approve
		law_parliamentary_republic = approve
		law_monarchy = disapprove
		law_magocracy = disapprove
		law_theocracy = strongly_disapprove
		law_council_republic = strongly_disapprove
	}
	lawgroup_citizenship = {
		law_ethnostate = approve
		law_national_supremacy = strongly_approve
		law_racial_segregation = approve
		law_cultural_exclusion = neutral
		law_multicultural = disapprove	
	}
	lawgroup_migration = {
		law_closed_borders = approve
		law_migration_controls = strongly_approve
		law_no_migration_controls = disapprove
	}
}

ideology_kheionist = {
	icon = "gfx/interface/icons/ideology_icons/anti_clerical.dds"
	lawgroup_church_and_state = {
		law_state_religion = strongly_disapprove
		law_freedom_of_conscience = disapprove
		law_total_separation = neutral
		law_state_atheism = strongly_approve
	}
	lawgroup_bureaucracy = {
		law_hereditary_bureaucrats = disapprove
		law_appointed_bureaucrats = neutral
		law_elected_bureaucrats = approve
	}
	lawgroup_education_system = {
		law_religious_schools = disapprove
		law_private_schools = strongly_approve
		law_no_schools = disapprove
		law_public_schools = approve		
	}
}