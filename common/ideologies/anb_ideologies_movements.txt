ideology_eordand_unification_movement = {
	icon = "gfx/interface/icons/ideology_icons/sovereignist.dds"

	lawgroup_church_and_state = {
		law_state_religion = neutral
		law_freedom_of_conscience = strongly_approve
		law_total_separation = disapprove
		law_state_atheism = strongly_disapprove
	}
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = disapprove
		law_racial_segregation = approve
		law_cultural_exclusion = neutral
		law_multicultural = disapprove
	}
	
	lawgroup_migration = {
		law_closed_borders = strongly_disapprove
		law_migration_controls = approve
		law_no_migration_controls = neutral
	}
}
ideology_pluralist_movement_3 = { # Cultural Exclusion
	icon = "gfx/interface/icons/ideology_icons/reformer.dds"
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = strongly_disapprove
		law_racial_segregation = disapprove
		law_cultural_exclusion = approve
		law_multicultural = neutral
	}
	lawgroup_racial_tolerance = {
		law_same_heritage_only = strongly_disapprove
		law_local_tolerance = approve
		law_expanded_tolerance = strongly_approve
		law_all_heritage = neutral
	}
}
ideology_pluralist_movement_4 = { # Multicultural
	icon = "gfx/interface/icons/ideology_icons/reformer.dds"
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = strongly_disapprove
		law_racial_segregation = disapprove
		law_cultural_exclusion = disapprove
		law_multicultural = approve
	}
	lawgroup_racial_tolerance = {
		law_same_heritage_only = strongly_disapprove
		law_local_tolerance = strongly_disapprove
		law_expanded_tolerance = approve
		law_all_heritage = strongly_approve
	}
}