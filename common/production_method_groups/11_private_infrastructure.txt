﻿pmg_base_building_railway = { 
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_sparkdrive_locomotives #Anbennar
		pm_steam_trains
		pm_steam_trains_principle_transport_3
		pm_electric_trains
		pm_electric_trains_principle_transport_3
		pm_diesel_trains # Anbennar renamed to Heavy Oil
		pm_diesel_trains_principle_transport_3
	}
}

pmg_passenger_trains = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	ai_selection = most_productive

	production_methods = {
		pm_no_passenger_trains
		pm_wooden_passenger_carriages
		pm_steel_passenger_carriages
	}
}

pmg_automation_railways = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_automata_laborers_railways # Anbennar
		pm_automata_machinists_railways # Anbennar
		pm_automata_laborers_railways_enforced # Anbennar
		pm_automata_machinists_railways_enforced # Anbennar
	}
}

pmg_base_building_trade_center = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_trade_center
		pm_trade_center_principle_external_trade_2
	}
}

pmg_ownership_building_trade_center = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_trade_center_merchant_guilds
		pm_trade_center_privately_owned
		pm_trade_center_bureaucrat_ownership
	}
}

pmg_ownership_building_manor_house = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_manor_house_arcane_aristocrats #anbennar
		pm_manor_house_privately_owned
		pm_manor_house_aristocrat_mage_principle_divine_economics_2 #anbennar
		pm_manor_house_principle_divine_economics_2
		pm_manor_house_mage_owned_principle_magical_economics #anbennar
	}
}

pmg_additional_ownership_building_manor_house = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_manor_house_clergy_ownership
		pm_manor_house_bureaucrat_ownership
	}
}

pmg_ownership_building_financial_district = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_financial_district_privately_owned
		pm_financial_district_publicly_traded
		pm_financial_district_principle_divine_economics_2
		pm_financial_district_principle_magical_economics_3
	}
}

pmg_ownership_building_company_headquarter = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	ai_selection = most_productive

	production_methods = {
		pm_company_headquarter_privately_owned
		pm_company_headquarter_mage_owned_principle_magical_economics_3
	}
}
