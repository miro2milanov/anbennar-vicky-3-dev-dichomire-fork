pmg_base_building_arcane_nexus = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_traditional_mage_tower
		pm_arcane_scholar_network
		pm_masters_of_magic
		pm_modern_mages
	}
}

pmg_focus_building_arcane_nexus = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_traditional_focus_building_arcane_nexus
		pm_mechanized_workshop_building_arcane_nexus
		pm_drudic_workshop_building_arcane_nexus
		pm_industrial_management_building_arcane_nexus
		pm_industrial_technothaumaturgie_building_arcane_nexus
		pm_artificer_acendency_building_arcane_nexus
		pm_marriage_arcane_artifice_building_arcane_nexus
	}
}