﻿no_production_spell = {
	icon = "gfx/interface/icons/magic/spells/no_production_spell.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
no_military_spell = {
	icon = "gfx/interface/icons/magic/spells/no_military_spell.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
no_society_spell = {
	icon = "gfx/interface/icons/magic/spells/no_society_spell.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}

#############Military spells#################

spell_evocation_corps = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_mageshields = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_spellknights = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_foreseen_battle = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_undead_army = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_master_of_the_plains = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_earthshaper_fortifications = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_vicious_chants = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_natures_gift = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_planewalker_saboteur = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_one_with_the_tree = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_chi_warriors = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_imbued_armor = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_aakhet_flames_calamity = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_neferkhet_incantation_dust = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_amiskhet_summoning_gale = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}

#############Production spells#################

spell_deposit_divination = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_spellwoven_luxury = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_magical_arts = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_mass_plant_growth = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_enchanted_tools = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_animal_companionship = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_attuned_forestery = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_roadshaper_wavechanger = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_storm_regulation = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_inscribed_architecture = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_ritualized_chi_siphoning = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_forge_and_flame = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_magical_automata = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_horutep_lash_dominance = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_shurket_elevation_sorrow = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_baqtkhet_sculpting_land = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}

#############Society spells#################

spell_perfected_terrain = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_magical_feasts = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_magical_authority = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_transmuted_gold_reserves = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_unleashed_chaos = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_grandstanding = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_divining_intent = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_veneration_of_tradition = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_portable_runes = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_social_sanctuaries = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_modern_miracle = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
# spell_inner_spirit = {
# 	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
# 	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
# 	modifier = {
# 	}
# }
spell_rituals_of_empathy = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_elikhet_gaze_judgement = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_nirakhet_breath_life = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}
spell_shurkhet_voice_wisdom = {
	icon = "gfx/interface/icons/magic/spells/placeholder.dds"
	background_texture = "gfx/interface/illustrations/institutions/colonization.dds"
	modifier = {
	}
}