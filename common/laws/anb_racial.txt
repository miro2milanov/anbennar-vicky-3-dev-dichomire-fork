﻿law_same_heritage_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/ethnostate.dds"
	
	on_enact = {
		recalculate_pop_ig_support = yes
		if = {
			limit = { country_has_primary_culture = all_heritage_tolerance }
			remove_primary_culture = cu:all_heritage_tolerance
		}
		if = {
			limit = { var:tolerance_group = flag:cannorian }
			remove_primary_culture = cu:cannorian_tolerance
			remove_primary_culture = cu:cannorian_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:bulwari }
			remove_primary_culture = cu:bulwari_tolerance
			remove_primary_culture = cu:bulwari_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:sarhali }
			remove_primary_culture = cu:sarhali_tolerance
			remove_primary_culture = cu:sarhali_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:fp }
			remove_primary_culture = cu:fp_tolerance
			remove_primary_culture = cu:fp_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:rahen }
			remove_primary_culture = cu:rahen_tolerance
			remove_primary_culture = cu:rahen_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:haless }
			remove_primary_culture = cu:haless_tolerance
			remove_primary_culture = cu:haless_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:gerudian }
			remove_primary_culture = cu:gerudian_tolerance
			remove_primary_culture = cu:gerudian_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:aelantiri }
			remove_primary_culture = cu:aelantiri_tolerance
			remove_primary_culture = cu:aelantiri_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_dwarf }
			remove_primary_culture = cu:dwarovar_dwarf_tolerance
			remove_primary_culture = cu:dwarovar_dwarf_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_goblin }
			remove_primary_culture = cu:dwarovar_goblin_tolerance
			remove_primary_culture = cu:dwarovar_goblin_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_orc }
			remove_primary_culture = cu:dwarovar_orc_tolerance
			remove_primary_culture = cu:dwarovar_orc_tolerance_plus
		}
	}
	
	modifier = {
		country_authority_add = 200
	}
	
	possible_political_movements = {
		law_local_tolerance
		law_local_tolerance_expanded
		law_all_heritage
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_local_tolerance = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/racial_segregation.dds"
	
	modifier = {
		country_authority_add = 100
	}
	
	possible_political_movements = {
		law_same_heritage_only
		law_local_tolerance_expanded
		law_all_heritage
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		if = {
			limit = { country_has_primary_culture = all_heritage_tolerance }
			remove_primary_culture = cu:all_heritage_tolerance
		}
		if = {
			limit = { var:tolerance_group = flag:cannorian }
			remove_primary_culture = cu:cannorian_tolerance_plus
			add_primary_culture = cu:cannorian_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:bulwari }
			remove_primary_culture = cu:bulwari_tolerance_plus
			add_primary_culture = cu:bulwari_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:sarhali }
			remove_primary_culture = cu:sarhali_tolerance_plus
			add_primary_culture = cu:sarhali_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:fp }
			remove_primary_culture = cu:fp_tolerance_plus
			add_primary_culture = cu:fp_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:rahen }
			remove_primary_culture = cu:rahen_tolerance_plus
			add_primary_culture = cu:rahen_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:haless }
			remove_primary_culture = cu:haless_tolerance_plus
			add_primary_culture = cu:haless_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:gerudian }
			remove_primary_culture = cu:gerudian_tolerance_plus
			add_primary_culture = cu:gerudian_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:aelantiri }
			remove_primary_culture = cu:aelantiri_tolerance_plus
			add_primary_culture = cu:aelantiri_tolerance
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_dwarf }
			add_primary_culture = cu:dwarovar_dwarf_tolerance
			remove_primary_culture = cu:dwarovar_dwarf_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_goblin }
			add_primary_culture = cu:dwarovar_goblin_tolerance
			remove_primary_culture = cu:dwarovar_goblin_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_orc }
			add_primary_culture = cu:dwarovar_orc_tolerance
			remove_primary_culture = cu:dwarovar_orc_tolerance_plus
		}
	}
	
}

law_expanded_tolerance = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/cultural_exclusion.dds"
	
	modifier = {
		country_authority_add = 50
	}
	
	unlocking_laws = {
		law_slavery_banned
		law_debt_slavery
	}
	
	possible_political_movements = {
		law_local_tolerance
		law_same_heritage_only
		law_all_heritage
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		if = {
			limit = { country_has_primary_culture = all_heritage_tolerance }
			remove_primary_culture = cu:all_heritage_tolerance
		}
		if = {
			limit = { var:tolerance_group = flag:cannorian }
			remove_primary_culture = cu:cannorian_tolerance
			add_primary_culture = cu:cannorian_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:bulwari }
			remove_primary_culture = cu:bulwari_tolerance
			add_primary_culture = cu:bulwari_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:sarhali }
			remove_primary_culture = cu:sarhali_tolerance
			add_primary_culture = cu:sarhali_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:fp }
			remove_primary_culture = cu:fp_tolerance
			add_primary_culture = cu:fp_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:rahen }
			remove_primary_culture = cu:rahen_tolerance
			add_primary_culture = cu:rahen_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:haless }
			remove_primary_culture = cu:haless_tolerance
			add_primary_culture = cu:haless_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:gerudian }
			remove_primary_culture = cu:gerudian_tolerance
			add_primary_culture = cu:gerudian_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:aelantiri }
			remove_primary_culture = cu:aelantiri_tolerance
			add_primary_culture = cu:aelantiri_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_dwarf }
			remove_primary_culture = cu:dwarovar_dwarf_tolerance
			add_primary_culture = cu:dwarovar_dwarf_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_goblin }
			remove_primary_culture = cu:dwarovar_goblin_tolerance
			add_primary_culture = cu:dwarovar_goblin_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_orc }
			remove_primary_culture = cu:dwarovar_orc_tolerance
			add_primary_culture = cu:dwarovar_orc_tolerance_plus
		}
	}
	
}

law_all_heritage = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/multicultural.dds"
	
	unlocking_laws = {
		law_slavery_banned
	}
	
	unlocking_technologies = {
		human_rights
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
		if = {
			limit = { var:tolerance_group = flag:cannorian }
			remove_primary_culture = cu:cannorian_tolerance
			remove_primary_culture = cu:cannorian_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:bulwari }
			remove_primary_culture = cu:bulwari_tolerance
			remove_primary_culture = cu:bulwari_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:sarhali }
			remove_primary_culture = cu:sarhali_tolerance
			remove_primary_culture = cu:sarhali_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:fp }
			remove_primary_culture = cu:fp_tolerance
			remove_primary_culture = cu:fp_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:rahen }
			remove_primary_culture = cu:rahen_tolerance
			remove_primary_culture = cu:rahen_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:haless }
			remove_primary_culture = cu:haless_tolerance
			remove_primary_culture = cu:haless_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:gerudian }
			remove_primary_culture = cu:gerudian_tolerance
			remove_primary_culture = cu:gerudian_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:aelantiri }
			remove_primary_culture = cu:aelantiri_tolerance
			remove_primary_culture = cu:aelantiri_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_dwarf }
			remove_primary_culture = cu:dwarovar_dwarf_tolerance
			remove_primary_culture = cu:dwarovar_dwarf_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_goblin }
			remove_primary_culture = cu:dwarovar_goblin_tolerance
			remove_primary_culture = cu:dwarovar_goblin_tolerance_plus
		}
		else_if = {
			limit = { var:tolerance_group = flag:dwarovar_orc }
			remove_primary_culture = cu:dwarovar_orc_tolerance
			remove_primary_culture = cu:dwarovar_orc_tolerance_plus
		}
		add_primary_culture = cu:all_heritage_tolerance
	}
	
	possible_political_movements = {
		law_local_tolerance
		law_local_tolerance_expanded
		law_same_heritage_only
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { 
						NOT = { has_law = law_type:law_all_races_allowed }
					}
				}
				add = 0.5				
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { NOT = { has_law = law_type:law_all_races_allowed } }
					standard_of_living <= 10 
				}
				add = 0.5				
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}