﻿

dna_adeline_sil_rubenaire = {
    portrait_info = {
    genes={         
  hair_color={ 255 21 222 180 }
 		skin_color={ 65 0 65 0 }
 		eye_color={ 131 160 209 152 }
 		gene_cheek_fat={ "cheek_fat" 144 "cheek_fat" 131 }
 		gene_cheek_forward={ "cheek_forward" 132 "cheek_forward" 132 }
 		gene_cheek_height={ "cheek_height" 209 "cheek_height" 133 }
 		gene_cheek_prom={ "cheek_prom" 221 "cheek_prom" 119 }
 		gene_cheek_width={ "cheek_width" 140 "cheek_width" 127 }
 		gene_chin_forward={ "chin_forward" 131 "chin_forward" 131 }
 		gene_chin_height={ "chin_height" 201 "chin_height" 119 }
 		gene_chin_width={ "chin_width" 118 "chin_width" 118 }
 		gene_ear_angle={ "ear_angle" 178 "ear_angle" 178 }
 		gene_ear_inner_shape={ "ear_inner_shape" 129 "ear_inner_shape" 129 }
 		gene_ear_lower_bend={ "ear_lower_bend" 52 "ear_lower_bend" 52 }
 		gene_ear_out={ "ear_out" 79 "ear_out" 79 }
 		gene_ear_size={ "ear_size" 122 "ear_size" 122 }
 		gene_ear_upper_bend={ "ear_upper_bend" 126 "ear_upper_bend" 126 }
 		gene_eye_angle={ "eye_angle" 175 "eye_angle" 120 }
 		gene_eye_depth={ "eye_depth" 147 "eye_depth" 147 }
 		gene_eye_distance={ "eye_distance" 120 "eye_distance" 120 }
 		gene_eye_height={ "eye_height" 143 "eye_height" 143 }
 		gene_eye_shut={ "eye_shut" 114 "eye_shut" 114 }
 		gene_eye_corner_def={ "eye_corner_def" 139 "eye_corner_def" 139 }
 		gene_eye_corner_depth_min={ "eye_corner_depth_min" 150 "eye_corner_depth_min" 150 }
 		gene_eye_fold_droop={ "eye_fold_droop" 144 "eye_fold_droop" 144 }
 		gene_eye_fold_shape={ "eye_fold_shape" 132 "eye_fold_shape" 132 }
 		gene_eye_size={ "eye_size" 193 "eye_size" 123 }
 		gene_eye_upper_lid_size={ "eye_upper_lid_size" 119 "eye_upper_lid_size" 119 }
 		gene_forehead_angle={ "forehead_angle" 129 "forehead_angle" 129 }
 		gene_forehead_brow_curve={ "forehead_brow_curve" 61 "forehead_brow_curve" 61 }
 		gene_forehead_brow_forward={ "forehead_brow_forward" 137 "forehead_brow_forward" 137 }
 		gene_forehead_brow_height={ "forehead_brow_height" 133 "forehead_brow_height" 133 }
 		gene_forehead_brow_inner_height={ "forehead_brow_inner_height" 120 "forehead_brow_inner_height" 120 }
 		gene_forehead_brow_outer_height={ "forehead_brow_outer_height" 134 "forehead_brow_outer_height" 134 }
 		gene_forehead_brow_width={ "forehead_brow_width" 117 "forehead_brow_width" 117 }
 		gene_forehead_height={ "forehead_height" 122 "forehead_height" 122 }
 		gene_forehead_roundness={ "forehead_roundness" 123 "forehead_roundness" 123 }
 		gene_forehead_width={ "forehead_width" 114 "forehead_width" 114 }
 		gene_head_height={ "head_height" 138 "head_height" 138 }
 		gene_head_profile={ "head_profile" 151 "head_profile" 151 }
 		gene_head_top_height={ "head_top_height" 131 "head_top_height" 131 }
 		gene_head_top_width={ "head_top_width" 64 "head_top_width" 64 }
 		gene_head_width={ "head_width" 125 "head_width" 125 }
 		gene_jaw_angle={ "jaw_angle" 74 "jaw_angle" 74 }
 		gene_jaw_def={ "jaw_def" 167 "jaw_def" 167 }
 		gene_jaw_forward={ "jaw_forward" 126 "jaw_forward" 126 }
 		gene_jaw_height={ "jaw_height" 141 "jaw_height" 122 }
 		gene_jaw_width={ "jaw_width" 62 "jaw_width" 119 }
 		gene_mouth_corner_height={ "mouth_corner_height" 144 "mouth_corner_height" 125 }
 		gene_mouth_forward={ "mouth_forward" 179 "mouth_forward" 84 }
 		gene_mouth_height={ "mouth_height" 150 "mouth_height" 119 }
 		gene_mouth_lower_lip_def={ "mouth_lower_lip_def" 117 "mouth_lower_lip_def" 130 }
 		gene_mouth_lower_lip_full={ "mouth_lower_lip_full" 137 "mouth_lower_lip_full" 120 }
 		gene_mouth_lower_lip_pads={ "mouth_lower_lip_pads" 138 "mouth_lower_lip_pads" 129 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size" 149 "mouth_lower_lip_size" 130 }
 		gene_mouth_lower_lip_width={ "mouth_lower_lip_width" 156 "mouth_lower_lip_width" 131 }
 		gene_mouth_open={ "mouth_open" 130 "mouth_open" 122 }
 		gene_mouth_philtrum_curve={ "mouth_philtrum_curve" 87 "mouth_philtrum_curve" 87 }
 		gene_mouth_philtrum_def={ "mouth_philtrum_def" 131 "mouth_philtrum_def" 131 }
 		gene_mouth_philtrum_width={ "mouth_philtrum_width" 127 "mouth_philtrum_width" 127 }
 		gene_mouth_upper_lip_curve={ "mouth_upper_lip_curve" 50 "mouth_upper_lip_curve" 215 }
 		gene_mouth_upper_lip_def={ "mouth_upper_lip_def" 127 "mouth_upper_lip_def" 141 }
 		gene_mouth_upper_lip_full={ "mouth_upper_lip_full" 201 "mouth_upper_lip_full" 103 }
 		gene_mouth_upper_lip_width={ "mouth_upper_lip_width" 146 "mouth_upper_lip_width" 129 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size" 182 "mouth_upper_lip_size" 139 }
 		gene_mouth_width={ "mouth_width" 144 "mouth_width" 134 }
 		gene_nose_curve={ "nose_curve" 75 "nose_curve" 75 }
 		gene_nose_forward={ "nose_forward" 116 "nose_forward" 116 }
 		gene_nose_hawk={ "nose_hawk" 122 "nose_hawk" 122 }
 		gene_nose_height={ "nose_height" 101 "nose_height" 101 }
 		gene_nose_length={ "nose_length" 124 "nose_length" 124 }
 		gene_nose_nostril_angle={ "nose_nostril_angle" 126 "nose_nostril_angle" 126 }
 		gene_nose_nostril_height={ "nose_nostril_height" 134 "nose_nostril_height" 134 }
 		gene_nose_nostril_width={ "nose_nostril_width" 124 "nose_nostril_width" 124 }
 		gene_nose_ridge_angle={ "nose_ridge_angle" 115 "nose_ridge_angle" 115 }
 		gene_nose_ridge_def={ "nose_ridge_def" 114 "nose_ridge_def" 114 }
 		gene_nose_ridge_def_min={ "nose_ridge_def_min" 68 "nose_ridge_def_min" 68 }
 		gene_nose_ridge_width={ "nose_ridge_width" 132 "nose_ridge_width" 132 }
 		gene_nose_size={ "nose_size" 139 "nose_size" 139 }
 		gene_nose_tip_angle={ "nose_tip_angle" 199 "nose_tip_angle" 199 }
 		gene_nose_tip_forward={ "nose_tip_forward" 128 "nose_tip_forward" 128 }
 		gene_nose_tip_width={ "nose_tip_width" 115 "nose_tip_width" 115 }
 		gene_neck_length={ "neck_length" 108 "neck_length" 108 }
 		gene_neck_width={ "neck_width" 134 "neck_width" 134 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 124 "body_fat_head_fat_full" 124 }
 		gene_height={ "normal_height" 167 "normal_height" 167 }
 		gene_age={ "old_1" 127 "old_1" 127 }
 		gene_old_ears={ "old_ears_01" 127 "old_ears_01" 127 }
 		gene_old_eyes={ "old_eyes_03" 255 "old_eyes_03" 255 }
 		gene_old_forehead={ "old_forehead_01" 255 "old_forehead_01" 255 }
 		gene_old_mouth={ "old_mouth_02" 255 "old_mouth_02" 255 }
 		gene_old_nose={ "old_nose_01" 127 "old_nose_01" 127 }
 		gene_complexion={ "complexion_01" 0 "complexion_03" 0 }
 		gene_stubble={ "stubble_low" 127 "stubble_low" 127 }
 		gene_crowfeet={ "crowfeet_01" 25 "crowfeet_01" 25 }
 		gene_face_dacals={ "face_dacal_01" 255 "face_dacal_01" 255 }
 		gene_frown={ "frown_03" 9 "frown_03" 9 }
 		gene_surprise={ "surprise_02" 25 "surprise_02" 25 }
 		gene_eyebrows_shape={ "avg_spacing_avg_thickness" 252 "far_spacing_avg_thickness" 252 }
 		gene_eyebrows_fullness={ "layer_2_avg_thickness" 157 "layer_2_avg_thickness" 157 }
 		hairstyles={ "european_hairstyles" 176 "european_hairstyles" 176 }
 		beards={ "no_beard" 208 "no_beard" 208 }
 		mustaches={ "no_mustache" 241 "no_mustache" 241 }
 		props={ "no_prop" 0 "no_prop" 0 }
 		eye_accessory={ "normal_eyes" 0 "normal_eyes" 0 }
 		eye_lashes_accessory={ "normal_eyelashes" 0 "normal_eyelashes" 0 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		POD_NOSschnoz={ "nosies" 0 "nosies" 0 }
 		race_gene_mer_ears_01={ "default_1" 127 "default_1" 127 }
 		race_gene_mer_ears_02={ "default_1" 127 "default_1" 127 }
 }

}

    enabled=yes
}
