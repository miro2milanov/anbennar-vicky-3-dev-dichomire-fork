﻿ig_trait_self_reliance = {
	icon = "gfx/interface/icons/ig_trait_icons/tax_avoidance.dds"
	max_approval = unhappy
	
	modifier = {
		country_trade_route_quantity_mult = -0.25
	}
}








#Konolkhatep
ig_trait_ahati_contributions = {
	icon = "gfx/interface/icons/ig_trait_icons/treasury_bonds.dds"
	min_approval = loyal
	
	modifier = {
		state_shopkeepers_investment_pool_efficiency_mult = 0.2
		country_trade_route_competitiveness_mult = 0.15
	}
}

ig_trait_ahati_loans = {
	icon = "gfx/interface/icons/ig_trait_icons/middle_managers.dds"
	min_approval = happy
	
	modifier = {
		country_loan_interest_rate_mult = -0.075
	}
}

ig_trait_independent_ahati = {
	icon = "gfx/interface/icons/ig_trait_icons/xenophobia.dds"
	max_approval = unhappy
	
	modifier = {
		state_radicalism_increases_violent_hostility_mult = 0.10
		state_radicalism_increases_cultural_erasure_mult = 0.10
		state_radicalism_increases_open_prejudice_mult = 0.10
		building_group_bg_agriculture_tax_mult = -0.05
		building_group_bg_plantations_tax_mult = -0.05
	}
}