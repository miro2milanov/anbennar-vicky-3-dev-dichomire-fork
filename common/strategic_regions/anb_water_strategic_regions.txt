﻿
region_dameshead = {
	states = { STATE_DAMESHEAD }
}

region_damesneck = {
	states = { STATE_DAMESNECK }
}

region_west_diven = {
	states = { STATE_WEST_DIVEN }
}

region_divengate = {
	states = { STATE_DIVENGATE }
}

region_central_diven = {
	states = { STATE_CENTRAL_DIVEN }
}

region_tefkora_pass = {
	states = { STATE_TEFKORA_PASS }
}

region_east_diven = {
	states = { STATE_EAST_DIVEN }
}

region_bay_of_wines = {
	states = { STATE_BAY_OF_WINES }
}

region_coast_of_venail = {
	states = { STATE_COAST_OF_VENAIL }
}

region_westcoast = {
	states = { STATE_WESTCOAST }
}

region_giants_grave_sea = {
	states = { STATE_GIANTS_GRAVE_SEA }
}

region_frozen_pass = {
	states = { STATE_FROZEN_PASS }
}


region_north_reavers_sea = {
	states = { STATE_NORTH_REAVERS_SEA }
}

region_south_reavers_sea = {
	states = { STATE_SOUTH_REAVERS_SEA }
}

region_northern_thaw = {
	states = { STATE_NORTHERN_THAW }
}

region_whaleroad = {
	states = { STATE_WHALEROAD }
}

region_lonely_isle_sea = {
	states = { STATE_LONELY_ISLE_SEA }
}

region_dragons_breath_sea = {
	states = { STATE_DRAGONS_BREATH_SEA }
}

region_lonely_far_lane_3 = {
	states = { STATE_LONELY_FAR_LANE_3 }
}

region_far_isle_coast = {
	states = { STATE_FAR_ISLE_COAST }
}

region_far_isle_coast_lane_1 = {
	states = { STATE_FAR_ISLE_COAST_LANE_1 }
}

region_far_isle_coast_lane_2 = {
	states = { STATE_FAR_ISLE_COAST_LANE_2 }
}
#ruined sea ish

region_ruined_sea = {
	states = { STATE_RUINED_SEA }
}

region_devils_lane = {
	states = { STATE_DEVILS_LANE }
}

region_edillions_relief = {
	states = { STATE_EDILLIONS_RELIEF }
}

region_edillions_relief_lane = {
	states = { STATE_EDILLIONS_RELIEF_LANE }
}
region_uelos_lament_1 = {
	states = { STATE_UELOS_LAMENT_1 }
}
region_uelos_lament_2 = {
	states = { STATE_UELOS_LAMENT_2 }
}
# region_uelos_lament_3 = {
# 	states = { STATE_UELOS_LAMENT_3 }
# }
region_uelos_lament_4 = {
	states = { STATE_UELOS_LAMENT_4 }
}
region_uelos_lament_5 = {
	states = { STATE_UELOS_LAMENT_5 }
}
region_uelos_lament_6 = {
	states = { STATE_UELOS_LAMENT_6 }
}

region_south_uelos_lament_1 = {
	states = { STATE_SOUTH_UELOS_LAMENT_1 }
}

region_south_uelos_lament_2 = {
	states = { STATE_SOUTH_UELOS_LAMENT_2 }
}

region_south_uelos_lament_3 = {
	states = { STATE_SOUTH_UELOS_LAMENT_3 }
}

region_banished_sea = {
	states = { STATE_BANISHED_SEA }
}

region_silsensald_sea = {
	states = { STATE_SILSENSALD_SEA }
}

region_calamity_pass = {
	states = { STATE_CALAMITY_PASS }
}

region_endrals_way = {
	states = { STATE_ENDRALS_WAY }
}

region_ochchus_sea = {
	states = { STATE_OCHCHUS_SEA }
}

region_reapers_coast = {
	states = { STATE_REAPERS_COAST }
}

region_sea_of_respite = {
	states = { STATE_SEA_OF_RESPITE }
}

region_trollsbay_coast = {
	states = { STATE_TROLLSBAY_COAST }
}
#North Aelantir
region_lovers_pass = {
	states = { STATE_LOVERS_PASS }
}
region_torn_sea = {
	states = { STATE_TORN_SEA }
}
region_artificers_coast = {
	states = { STATE_ARTIFICERS_COAST }
}
region_gulf_of_neanaani = {
	states = { STATE_GULF_OF_NEANAANI }
}
region_sarmadfar = {
	states = { STATE_SARMADFAR }
}
region_fogharabh = {
	states = { STATE_FOGHARABH }
}
region_eigras = {
	states = { STATE_EIGRAS }
}
region_torriaran = {
	states = { STATE_TORRIARAN }
}
region_bay_of_verencel = {
	states = { STATE_BAY_OF_VERENCEL }
}
region_eor_islands_sea = {
	states = { STATE_EOR_ISLANDS_SEA }
}
region_eastern_floeway = {
	states = { STATE_EASTERN_FLOEWAY }
}
region_western_floeway = {
	states = { STATE_WESTERN_FLOEWAY  }
}
region_icebreaker_pass = {
	states = { STATE_ICEBREAKER_PASS }
}
region_iceberg_alley = {
	states = { STATE_ICEBERG_ALLEY }
}
region_crownsman_current = {
	states = { STATE_CROWNSMANS_CURRENT }
}
#Sarhal
region_fahvanosy_sea = {
	states = { STATE_FAHVANOSY_SEA }
}
region_dao_nako_coast = {
	states = { STATE_DAO_NAKO_COAST }
}
region_sandspite_sea = {
	states = { STATE_SANDSPITE_SEA }
}
region_voyagers_sea = {
	states = { STATE_VOYAGERS_SEA }
}
region_voyagers_sea_lane_1 = {
	states = { STATE_VOYAGERS_SEA_LANE_1 }
}
region_voyagers_sea_lane_2 = {
	states = { STATE_VOYAGERS_SEA_LANE_2 }
}
#South Aelantir
region_turtleback_sea = {
	states = { STATE_TURTLEBACK_SEA }
}
region_gleaming_sea = {
	states = { STATE_GLEAMING_SEA }
}
region_radiant_sea = {
	states = { STATE_RADIANT_SEA }
}
region_sea_of_ameion = {
	states = { STATE_SEA_OF_AMEION }
}
region_cleaved_sea = {
	states = { STATE_CLEAVED_SEA }
}
region_kaydhano_coast = {
	states = { STATE_KAYDHANO_COAST }
}
region_matni_sea = {
	states = { STATE_MATNI_SEA }
}
region_thenon_current = {
	states = { STATE_THENON_CURRENT }
}

region_voyagers_sea_lane_1 = {
	states = { STATE_VOYAGERS_SEA_LANE_1 }
}
region_wrecks_sea_lane_1 = {
	states = { STATE_WRECKS_SEA_LANE_1 }
}
#insyaa-aelantir edge zone
region_eltchamas_pass_1 = {
	states = { STATE_ELTCHAMAS_PASS_1 }
}
region_eltchamas_pass_2 = {
	states = { STATE_ELTCHAMAS_PASS_2 }
}
region_cleaved_sea_departure_lane = {
	states = { STATE_CLEAVED_SEA_DEPARTURE_LANE }
}
region_sea_of_silence_lane_1 = {
	states = { STATE_SEA_OF_SILENCE_LANE_1 }
}
region_sea_of_silence_lane_2 = {
	states = { STATE_SEA_OF_SILENCE_LANE_2 }
}
region_sea_of_silence_lane_3 = {
	states = { STATE_SEA_OF_SILENCE_LANE_3 }
}
region_sea_of_silence_lane_4 = {
	states = { STATE_SEA_OF_SILENCE_LANE_4 }
}
region_mudfaibh_pass_1 = {
	states = { STATE_MUDFAIBH_PASS_1 }
}
region_mudfaibh_pass_2 = {
	states = { STATE_MUDFAIBH_PASS_2 }
}
region_mudfaibh_pass_3 = {
	states = { STATE_MUDFAIBH_PASS_3 }
}
region_blathfar_pass_1 = {
	states = { STATE_BLATHFAR_PASS_1 }
}
region_blathfar_pass_2 = {
	states = { STATE_BLATHFAR_PASS_2 }
}
region_addersid_lane = {
	states = { STATE_ADDERSID_LANE }
}
#impassable
region_gathmarkorsk_sea = {
	states = { STATE_GATHMARKORSK_SEA }
}
region_torrieanach_sea = {
	states = { STATE_TORRIEANACH_SEA }
}
region_kelpforest_sea = {
	states = { STATE_KELPFOREST_SEA }
}
region_sea_of_silence = {
	states = { STATE_SEA_OF_SILENCE }
}
region_torn_sea_2 = {
	states = { STATE_TORN_SEA_2 }
}
region_torn_sea_3 = {
	states = { STATE_TORN_SEA_3 }
}
region_torn_sea_4 = {
	states = { STATE_TORN_SEA_4 }
}
region_jerkhich_sea = {
	states = { STATE_JERKHICH_SEA }
}
region_northern_ocean_of_the_lost = {
	states = { STATE_NORTHERN_OCEAN_OF_THE_LOST }
}
region_ocean_of_the_lost = {
	states = { STATE_OCEAN_OF_THE_LOST_2 }
}
region_reian_marlos = {
	states = { STATE_REIAN_MARLOS }
}
region_southern_ocean_of_the_lost_1 = {
	states = { STATE_SOUTHERN_OCEAN_OF_THE_LOST_1 }
}
region_southern_ocean_of_the_lost_2 = {
	states = { STATE_SOUTHERN_OCEAN_OF_THE_LOST_2 }
}

#Haless

region_east_sarhal_coast = {
	states = { STATE_EAST_SARHAL_COAST }
}
region_punctured_coast = {
	states = { STATE_PUNCTURED_COAST }
}
region_kedwali_coast = {
	states = { STATE_KEDWALI_COAST }
}
region_kedwali_gulf = {
	states = { STATE_KEDWALI_GULF }
}
region_ardimyan_sea = {
	states = { STATE_ARDIMYAN_SEA }
}
region_insyaa_sea_1 = {  
	states = { STATE_INSYAA_SEA_1 }
}
region_insyaa_sea_2 = { 
	states = { STATE_INSYAA_SEA_2 }
}
region_barrier_islands_sea = {
	states = { STATE_BARRIER_ISLANDS_SEA }
}
region_themsea = {
	states = { STATE_THEMSEA }
}

region_bay_of_insyaa = {
	states = { STATE_BAY_OF_INSYAA }
}
region_insyaa_sea_3 = {
	states = { STATE_INSYAA_SEA_3 }
}
region_jadd_sea = {
	states = { STATE_JADD_SEA }
}
region_gulf_of_rahen = {
	states = { STATE_GULF_OF_RAHEN }
}
region_khom_sea = {
	states = { STATE_KHOM_SEA }
}
region_ringlet_basin = {
	states = { STATE_RINGLET_BASIN  }
}
region_phokhao_pass = {
	states = { STATE_PHOKHAO_PASS }
}
region_jellyfish_coast = {
	states = { STATE_JELLYFISH_COAST }
}
region_odheonhgu_sea = {
	states = { STATE_ODHEONGU_SEA }
}
region_widows_sea = {
	states = { STATE_WIDOWS_SEA }
}
region_hokhos_sea = {
	states = { STATE_HOKHOS_SEA }
}
region_blue_sea = {
	states = { STATE_BLUE_SEA }
}
region_kodarve_lake = {
	states = { STATE_KODARVE_LAKE }
}
region_yukelqur_lake = { 
	states = { STATE_YUKELQUR_LAKE }
}
region_oriolg_channel = {
	states = { STATE_ORIOLG_CHANNEL }
}
region_zernuuk_lake = {
	states = { STATE_ZERNUUK_LAKE }
}

region_the_bridge = {
	states = { STATE_THE_BRIDGE }
}