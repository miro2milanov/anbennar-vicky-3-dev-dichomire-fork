﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

ruinborn_amadian = {
	template = "moon_elf"

	
	skin_color = {
        10 = { 0.3 0.25 0.5 0.35 }
		10 = { 0.3 0.35 0.5 0.49 }
	}
	hair_color = {
        # Black
        10 = { 0.01 0.95 0.97 0.99 }
	}

    gene_nose_size = {
        17 = { name = nose_size   range = { 0.4 0.5 }      }
        74 = { name = nose_size   range = { 0.5 0.55 }      }
        7 = { name = nose_size   range = { 0.55 0.65 }      }
    }

    gene_nose_nostril_width = {
        7 = { name = nose_nostril_width   range = { 0.4 0.45 }      }
        84 = { name = nose_nostril_width   range = { 0.45 0.55 }      }
        17 = { name = nose_nostril_width   range = { 0.55 0.65 }      }
    }


	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
	}

} 

half_ruinborn_amadian = {
	template = "half_moon_elf"

	
	skin_color = {
        5 = { 0.3 0.25 0.5 0.35 }
		1 = { 0.3 0.35 0.5 0.49 }
		1 = { 0.0 0.05 0.15 0.1 }  #human caucasian skin tone
		10 = { 0.15 0.05 0.5 0.49  }
	}
	hair_color = {
        # Black
        50 = { 0.01 0.95 0.97 0.99 }
		# Brown
		25 = { 0.65 0.5 0.9 0.8 }
		# Red
		10 = { 0.85 0.0 0.99 0.5 }
		# Blonde
		10 = { 0.4 0.25 0.75 0.5 }
	}

    gene_nose_size = {
        17 = { name = nose_size   range = { 0.4 0.5 }      }
        74 = { name = nose_size   range = { 0.5 0.55 }      }
        7 = { name = nose_size   range = { 0.55 0.65 }      }
    }

    gene_nose_nostril_width = {
        7 = { name = nose_nostril_width   range = { 0.4 0.45 }      }
        84 = { name = nose_nostril_width   range = { 0.45 0.55 }      }
        17 = { name = nose_nostril_width   range = { 0.55 0.65 }      }
    }


	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
		10 = { name = european_hairstyles 		range = { 0.0 1.0 } }
	}

} 
