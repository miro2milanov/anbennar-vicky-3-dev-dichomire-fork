﻿CHARACTERS = {
	c:B42 ?= {
		create_character = { # Crafty casino owner, seeking new and unique ways to enrich himself and the local political elite. Rumored to have been an outlaw in his younger days.
			first_name = Dennar
			last_name =  Merril
			historical = yes
			age = 43
			ruler = yes
			interest_group = ig_petty_bourgeoisie
			ig_leader = yes
			ideology = ideology_authoritarian
			traits = {
				meticulous innovative expensive_tastes bandit
			}
		}
	}
}
