﻿COUNTRIES = {
	c:C01 ?= {
		effect_starting_technology_tier_2_tech = yes
		add_technology_researched = tradition_of_equality
		
		# Laws 
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_charitable_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		
		activate_law = law_type:law_nation_of_artifice
	}
}