﻿COUNTRIES = {
	c:B60 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = central_archives
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_technocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_secret_police

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		# No police
		# No schools
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slave_trade
		
		activate_law = law_type:law_same_heritage_only
		
		activate_law = law_type:law_nation_of_artifice
	}
}