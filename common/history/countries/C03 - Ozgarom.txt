﻿COUNTRIES = {
	c:C03 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = empiricism
		add_technology_researched = tradition_of_equality
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_national_guard
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_dedicated_police
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance
		
		activate_law = law_type:law_nation_of_magic
	}
}