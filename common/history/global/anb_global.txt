﻿GLOBAL = {
	every_country = {
		limit = {
			NOT = { is_country_type = decentralized }
		}

		#every_scope_state = {
		#	set_quest_target = yes
		#}
		#add_journal_entry = { type = je_shadow_over_ynnsmouth }
		#trigger_event = {
		#	id = shadow_over_ynnsmouth.1
		#}

		#setup_racial_flags = yes
		
		trigger_event = {
			id = anb_initialization.1
			days = 1
		}
		
	}
	every_country = {
		initialize_traditions = yes
	
		initialize_spells = yes
		
		trigger_event = { id = anb_magical_expertise.1 days = 1 }
	}
}
