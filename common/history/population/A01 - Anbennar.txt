﻿POPULATION = {
	c:A01 = {	#Anbennar
		
		effect_starting_pop_wealth_high = yes
		effect_starting_pop_literacy_middling = yes
		
	}

	c:A02 = {	#Vivin Empire
		
		effect_starting_pop_wealth_high = yes
		effect_starting_pop_literacy_low = yes
		
	}

	c:A03 = {	#Lorent
		
		effect_starting_pop_wealth_very_high = yes
		effect_starting_pop_literacy_high = yes
		
	}

	c:A04 = {	#Northern League
		
		effect_starting_pop_wealth_high = yes
		effect_starting_pop_literacy_middling = yes
		
	}

	c:A05 = {	#Bisan
		
		effect_starting_pop_wealth_high = yes
		effect_starting_pop_literacy_high = yes
		
	}
}