﻿POPS = {
	s:STATE_KHARUNYANA_BOMDAN = {
		region_state:Y13 = {
			create_pop = {
				culture = chengrong
				size = 5000000
			}
		}
		region_state:Y12 = {
			create_pop = {
				culture = chengrong
				size = 5100000
			}
		}
		region_state:Y32 = {
			create_pop = {
				culture = chengrong
				size = 3920000
			}
		}
	}
	
	s:STATE_LOWER_TELEBEI = {
		region_state:Y32 = {
			create_pop = {
				culture = ranilau
				size = 1380000
			}
		}
	}
	
	s:STATE_BIM_LAU = {
		region_state:Y15 = {
			create_pop = {
				culture = ranilau
				size = 800000
			}
		}
		region_state:Y14 = {
			create_pop = {
				culture = ranilau
				size = 7100000
			}
		}
		region_state:Y32 = {
			create_pop = {
				culture = ranilau
				size = 160000
			}
		}
	}
	
	s:STATE_JIEZHONG = {
		region_state:R13 = {
			create_pop = {
				culture = kintonan
				size = 2424400
			}
			create_pop = {
				culture = serene_harimari
				size = 255200
				religion = righteous_path
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 255200
			}
			create_pop = {
				culture = feng_harpy
				size = 223300
			}
			create_pop = {
				culture = goldscale_kobold
				size = 31900
			}
		}
	}
	
	s:STATE_QIANZHAOLIN = {
		region_state:R13 = {
			create_pop = {
				culture = kintonan
				size = 939600
			}
			create_pop = {
				culture = serene_harimari
				size = 809100
				religion = righteous_path
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 495900
			}
			create_pop = {
				culture = feng_harpy
				size = 130500
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 182700
			}
			create_pop = {
				culture = goldscale_kobold
				size = 52200
			}
		}
	}
	
	s:STATE_WANGQIU = {
		region_state:R13 = {
			create_pop = {
				culture = kintonan
				size = 984900
			}
			create_pop = {
				culture = serene_harimari
				size = 147000
				religion = righteous_path
			}
			create_pop = {
				culture = elephant_hobgoblin
				size = 73500
			}
			create_pop = {
				culture = feng_harpy
				size = 88200
			}
			create_pop = {
				culture = wuhyun_half_orc
				size = 58800
			}
			create_pop = {
				culture = goldscale_kobold
				size = 117600
			}
		}
	}
	
	s:STATE_SIKAI = {
		region_state:Y17 = {
			create_pop = {
				culture = sikai
				size = 6308000
			}
			create_pop = {
				culture = royal_harimari
				size = 199200
			}
			create_pop = {
				culture = sunrise_elf
				size = 132800
			}
		}
	}
	
	s:STATE_DEKPHRE = {
		region_state:Y17 = {
			create_pop = {
				culture = teplin
				size = 1895500
			}
			create_pop = {
				culture = royal_harimari
				size = 223000
			}
			create_pop = {
				culture = swallow_hobgoblin
				size = 111500
			}
		}
	}
	
	s:STATE_KHINDI = {
		region_state:Y32 = {
			create_pop = {
				culture = risbeko
				size = 2500000
			}
			create_pop = {
				culture = biengdi
				size = 1
			}
		}
		region_state:Y16 = {
			create_pop = {
				culture = risbeko
				size = 700000
			}
			create_pop = {
				culture = biengdi
				size = 1
			}
		}
	}
	
	s:STATE_KHABTEI_TELENI = {
		region_state:Y16 = {
			create_pop = {
				culture = biengdi
				size = 4185600
			}
			create_pop = {
				culture = royal_harimari
				size = 174400
			}
		}
	}
	
	s:STATE_RONGBEK = {
		region_state:Y18 = {
			create_pop = {
				culture = risbeko
				size = 510000
			}
		}
	}
	
	s:STATE_HINPHAT = {
		region_state:Y20 = {
			create_pop = {
				culture = hinphat
				size = 2997000
			}
			create_pop = {
				culture = nephrite_dwarf
				size = 370000
			}
			create_pop = {
				culture = feng_harpy
				size = 333000
				religion = the_hunt
			}
		}
	}
	
	s:STATE_NAGON = {
		region_state:Y19 = {
			create_pop = {
				culture = gon
				size = 900000
			}
		}
		region_state:Y32 = {
			create_pop = {
				culture = gon
				size = 700000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = gon
				size = 110000
			}
		}
	}
	
	s:STATE_KHOM_MA = {
		region_state:Y19 = {
			create_pop = {
				culture = khom
				size = 9150000
			}
		}
	}
	
	s:STATE_PHONAN = {
		region_state:Y19 = {
			create_pop = {
				culture = phonan
				size = 2450000
			}
		}
	}
	
	s:STATE_HOANGDESINH = {
		region_state:A09 = {
			create_pop = {
				culture = pinghoi
				size = 500000
			}
			create_pop = {
				culture = paru
				size = 1
			}
		}
		region_state:Y19 = {
			create_pop = {
				culture = khom
				size = 2790000
			}
			create_pop = {
				culture = pinghoi
				size = 1
			}
			create_pop = {
				culture = paru
				size = 1
			}
		}
	}
	
	s:STATE_TLAGUKIT = {
		region_state:Y22 = {
			create_pop = {
				culture = hujan
				size = 100000
			}
			create_pop = {
				culture = sirtana
				size = 40000
			}
		}
		region_state:Y24 = {
			create_pop = {
				culture = banyak
				size = 300000
			}
			create_pop = {
				culture = hujan
				size = 300000
			}
		}
	}
	
	s:STATE_SIRTAN = {
		region_state:Y22 = {
			create_pop = {
				culture = sirtana
				size = 620000
			}
		}
	}
	
	s:STATE_KUDET_KAI = {
		region_state:Y33 = {
			create_pop = {
				culture = bokai
				size = 2500000
			}
		}
		region_state:Y21 = {
			create_pop = {
				culture = bokai
				size = 1620000
			}
		}
	}
	
	s:STATE_YEMAKAIBO = {
		region_state:Y33 = {
			create_pop = {
				culture = bokai
				size = 560000
			}
		}
	}
	
	s:STATE_ARAWKELIN = {
		region_state:Y23 = {
			create_pop = {
				culture = kelino
				size = 1560400
			}
			create_pop = {
				culture = royal_harimari
				size = 83000
			}
			create_pop = {
				culture = sunrise_elf
				size = 16600
				religion = high_philosophy
			}
		}
	}
	
	s:STATE_REWIRANG = {
		region_state:Y23 = {
			create_pop = {
				culture = paru
				size = 30000
			}
		}
		region_state:Y25 = {
			create_pop = {
				culture = paru
				size = 200000
			}
			create_pop = {
				culture = hujan
				size = 1
			}
		}
		region_state:Y26 = {
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
		region_state:Y27 = {
			create_pop = {
				culture = hujan
				size = 1
			}
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
		region_state:Y28 = {
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
		region_state:Y29 = {
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
	}
	
	s:STATE_MESATULEK = {
		region_state:Y23 = {
			create_pop = {
				culture = paru
				size = 90000
			}
		}
		region_state:Y33 = {
			create_pop = {
				culture = paru
				size = 300000
			}
		}
		region_state:Y30 = {
			create_pop = {
				culture = hujan
				size = 100000
			}
		}
		region_state:Y31 = {
			create_pop = {
				culture = hujan
				size = 100000
			}
			create_pop = {
				culture = banyak
				size = 1
			}
		}
	}
	
	s:STATE_NON_CHIEN = {
		region_state:Y33 = {
			create_pop = {
				culture = binhrung
				size = 1040000
			}
		}
	}
	
	s:STATE_BINHRUNGHIN = {
		region_state:Y33 = {
			create_pop = {
				culture = binhrung
				size = 2440000
			}
		}
	}
	
	s:STATE_VERKAL_OZOVAR = {
		region_state:Y20 = {
			create_pop = {
				culture = nephrite_dwarf
				size = 2870000
			}
			create_pop = {
				culture = hinphat
				size = 290000
			}
		}
	}
	
	s:STATE_TIPHIYA = {
		region_state:Y32 = {
			create_pop = {
				culture = pinghoi
				size = 500000
			}
		}
	}
	
	s:STATE_KIUBANG = {
		region_state:Y33 = {
			create_pop = {
				culture = pinghoi
				size = 500000
			}
			create_pop = {
				culture = gataw
				size = 1
			}
		}
	}
	
	s:STATE_GINHYUT = {
		region_state:Y33 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
	}
	
	s:STATE_LUNGDOU = {
		region_state:A04 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
	}
	
	s:STATE_LEOIHOIN = {
		region_state:Y33 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
	}
}