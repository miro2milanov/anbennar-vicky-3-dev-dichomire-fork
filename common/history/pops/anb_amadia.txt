﻿POPS = {
	s:STATE_BRONRIN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 342400
			}
			create_pop = {
				culture = costine
				size = 128400
			}
			create_pop = {
				culture = manamya
				size = 205440
				split_religion = {
					manamya = {
						ravelian = 0.8
						rioqay_diu = 0.2
					}
				}
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 42800
			}
			create_pop = {
				culture = havener_elf
				size = 17120
			}
			create_pop = {
				culture = silver_dwarf
				size = 25680
			}
			create_pop = {
				culture = imperial_gnome
				size = 8560
			}
			create_pop = {
				culture = beefoot_halfling
				size = 85600
			}
		}
	}
	s:STATE_NUR_ISTRALORE = {
		region_state:C02 = {
			create_pop = {
				culture = east_damerian
				size = 111000
				religion = corinite
			}
			create_pop = {
				culture = west_damerian
				size = 14800
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 10360
			}
			create_pop = {
				culture = manamya
				size = 5920
				split_religion = {
					manamya = {
						ravelian = 0.4
						corinite = 0.4
						rioqay_diu = 0.2
					}
				}
			}
			create_pop = {
				culture = havener_elf
				size = 1480
			}
			create_pop = {
				culture = beefoot_halfling
				size = 4440
			}
		}
	}
	s:STATE_MUNASIN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 113400
			}
			create_pop = {
				culture = costine
				size = 45360
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 37800
			}
			create_pop = {
				culture = havener_elf
				size = 10080
			}
			create_pop = {
				culture = beefoot_halfling
				size = 7560
			}
			create_pop = {
				culture = manamya
				size = 37800
				split_religion = {
					manamya = {
						ravelian = 0.8
						rioqay_diu = 0.2
					}
				}
			}
		}
	}
	s:STATE_SILVEGOR = {
		region_state:C02 = {
			create_pop = {
				culture = exwesser
				size = 103200
			}
			create_pop = {
				culture = manamya
				size = 36120
				split_religion = {
					manamya = {
						ravelian = 0.8
						rioqay_diu = 0.2
					}
				}
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 20640
			}
			create_pop = {
				culture = havener_elf
				size = 12040
			}
		}
	}
	s:STATE_CLAMGUINN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 66720
			}
			create_pop = {
				culture = manamya
				size = 211280
				split_religion = {
					manamya = {
						ravelian = 0.75
						rioqay_diu = 0.25
					}
				}
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 13900
			}
		}
	}
	s:STATE_URANCESTIR = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 36400
			}
			create_pop = {
				culture = manamya
				size = 114660
				split_religion = {
					manamya = {
						ravelian = 0.75
						rioqay_diu = 0.25
					}
				}
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 25480
			}
			create_pop = {
				culture = silver_dwarf
				size = 5460
			}
		}
	}
	s:STATE_CALILVAR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 12340
			}
			create_pop = {
				culture = manamya
				size = 143210
				split_religion = {
					manamya = {
						corinite = 0.8
						rioqay_diu = 0.2
					}
				}
			}
		}
	}
	s:STATE_CYMBEAHN = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 367920
			}
			create_pop = {
				culture = manamya
				size = 74592
				split_religion = {
					manamya = {
						corinite = 0.8
						rioqay_diu = 0.2
					}
				}
			}
			create_pop = {
				culture = east_damerian
				size = 25200
			}
			create_pop = {
				culture = silver_dwarf
				size = 30240
			}
			create_pop = {
				culture = imperial_gnome
				size = 1010
			}
			create_pop = {
				culture = beefoot_halfling
				size = 5040
			}
		}
	}
	s:STATE_OOMU_NELIR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 21420
			}
			create_pop = {
				culture = pearlsedger
				size = 39780
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 39820
			}
			create_pop = {
				culture = manamya
				size = 188190
				split_religion = {
					manamya = {
						corinite = 0.75
						rioqay_diu = 0.25
					}
				}
			}
		}
	}
	s:STATE_CARA_LAFQUEN = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 52200
			}
			create_pop = {
				culture = manamya
				size = 295800
				split_religion = {
					manamya = {
						corinite = 0.75
						rioqay_diu = 0.25
					}
				}
			}
		}
	}
	s:STATE_HARENAINE = {
		region_state:C11 = {
			create_pop = {
				culture = quruwei
				size = 107350
			}
			create_pop = {
				culture = vernman
				size = 5650
			}
		}
	}
	s:STATE_ARANLAS = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 66000
			}
			create_pop = {
				culture = east_damerian
				size = 72400
			}
			create_pop = {
				culture = quruwei
				size = 31600
				split_religion = {
					manamya = {
						corinite = 0.8
						rioqay_diu = 0.2
					}
				}
			}
			create_pop = {
				culture = south_aelantiri_orc
				size = 20320
			}
			create_pop = {
				culture = silver_dwarf
				size = 9120
			}
			create_pop = {
				culture = imperial_gnome
				size = 2640
			}
		}
		region_state:C11 = {
			create_pop = {
				culture = quruwei
				size = 33400
			}
		}
	}
}