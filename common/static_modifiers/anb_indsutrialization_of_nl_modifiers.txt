﻿nl_focused_region = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	state_loyalists_from_sol_change_mult = 0.10
	state_construction_mult = 0.20
}

nl_encouraged_proselytization_interest_group = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	interest_group_approval_add = 1
	interest_group_pol_str_mult = 0.1
}

nl_rejected_proselytization = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_negative.dds
	interest_group_approval_add = -1
}

nl_encouraged_proselytization = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_fist_positive.dds
	state_conversion_mult = 0.5
}

nl_encouraged_education = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_positive.dds
	state_education_access_add = 0.25
	state_conversion_mult = 0.1
}

modifier_nl_conversion_costs = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 1
}

nl_religious_crackdown = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_fist_positive.dds
	state_conversion_mult = 0.5
	state_mortality_turmoil_mult = 0.1
}

nl_negotiated_with_anti_religious_rebels = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_negative.dds
	interest_group_approval_add = -2
}

nl_embraced_anti_religious_rebels = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_negative.dds
	interest_group_approval_add = -2
}

nl_rejected_anti_religious_rebels = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	interest_group_approval_add = 1
}

supported_anti_religious_policies = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_fist_positive.dds
	conversion_mult = -0.25
	#loyalists_from_sol_change_mult = 0.05 ## modifier does not exist
}

nl_nationalist_support = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	interest_group_pop_attraction_mult = 0.15
}

nl_rejected_woodswaller_presence = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_gun_negative.dds
	interest_group_approval_add = -1
	interest_group_pol_str_mult = -0.1
}

nl_woodswaller_stronghold = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_fist_negative.dds
	state_tax_waste_add = 0.15
	state_construction_mult = -0.05
	state_infrastructure_add = -5
}

nl_woodswaller_stronghold_small = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_fist_negative.dds
	state_tax_waste_add = 0.10
	state_infrastructure_add = -5
}

nl_accepted_adshaw_assistance = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	interest_group_approval_add = 1
	interest_group_pol_str_mult = 0.2
}

nl_anti_woodswall_presence = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_gun_positive.dds
	state_mortality_mult = 0.05
}

nl_anti_woodswall_presence_costs = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 1
}

nl_anti_agriculture = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	#building_group_bg_agriculture_throughput_mult = -0.1 ## modifier does not exist
	#building_group_bg_plantations_throughput_mult = -0.1 ## modifier does not exist
}

nl_anti_resources = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	#building_group_bg_mining_throughput_mult = -0.1 ##
	#building_group_bg_logging_throughput_mult = -0.1 ## modifier does not exist
	#building_group_bg_fishing_throughput_mult = -0.1 ## modifier does not exist
	#building_group_bg_whaling_throughput_mult = -0.1 ## modifier does not exist
	#building_group_bg_oil_extraction_throughput_mult = -0.1 ## modifier does not exist
	#building_group_bg_rubber_throughput_mult = -0.1 ## modifier does not exist

}

nl_anti_industry = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	#building_group_bg_manufacturing_throughput_mult = -0.1 ## modifier does not exist
	#building_group_bg_service_throughput_mult = -0.1 ## modifier does not exist
}

nl_encouraged_manufacturing = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	#building_group_bg_manufacturing_throughput_mult = 0.1 ## modifier does not exist
	#building_group_bg_service_throughput_mult = 0.1 ## modifier does not exist
}

nl_encouraged_investment = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	state_construction_mult = 0.10
}

nl_promised_brighter_future = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_positive.dds
	state_radicals_and_loyalists_from_sol_change_mult = 0.10
}

nl_negotiated_with_woodswallers = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_negative.dds
	country_authority_mult = -0.05
}