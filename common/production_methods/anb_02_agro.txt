﻿## Farm Secondary Variants

#Serpentbloom Farms
pm_serpentbloom_no_secondary = {
	texture = "gfx/interface/icons/production_method_icons/no_orchards.dds"
	is_default = yes
}
pm_serpentbloom_opium = {
	texture = "gfx/interface/icons/production_method_icons/orchards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_grain_add = -10
			goods_output_opium_add = 10
		}
	}
}

#Mushroom Farms
pm_mushroom_no_secondary = {
	texture = "gfx/interface/icons/production_method_icons/no_orchards.dds"
	is_default = yes
}
pm_mushroom_liquor = {
	texture = "gfx/interface/icons/production_method_icons/potatoes.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_grain_add = -10
			goods_output_liquor_add = 15
		}
	}
}


## Farms

pm_herb_gardens = {
	texture = "gfx/interface/icons/production_method_icons/orchards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_grain_add = -10
			goods_output_magical_reagents_add = 10
		}
	}
}

pm_herb_gardens_rice_farm = {
	texture = "gfx/interface/icons/production_method_icons/orchards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_grain_add = -30
			goods_output_magical_reagents_add = 30
		}
	}
}

pm_growth_beans_farms = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_fertilizer_add = 5			
			goods_input_magical_reagents_add = 2				
			
			# output goods													
			goods_output_grain_add = 15
		}
	}
}

pm_high_velocity_irrigation_farms = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		gene_transmutation			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_fertilizer_add = 10			
			goods_input_artificery_doodads_add = 3				
			
			# output goods													
			goods_output_grain_add = 30
		}

		level_scaled = {
			# earnings	
			building_employment_laborers_add = -250			
			building_employment_machinists_add = 250
		}
	}
}

pm_chronoponics_farms = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"
	unlocking_technologies = {
		essence_extraction			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_fertilizer_add = 15			
			goods_input_artificery_doodads_add = 6				
			
			# output goods													
			goods_output_grain_add = 50
		}

		level_scaled = {
			# earnings	
			building_employment_laborers_add = -500		
			building_employment_machinists_add = 500
		}
	}
}

pm_automata_harvesters = {
	texture = "gfx/interface/icons/production_method_icons/steam_powered_threshers.dds"
	unlocking_technologies = {
		early_mechanim		
	}
	
	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_automata_add = 4
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_autonomous_tractors = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"

	unlocking_technologies = {
		advanced_mechanim			
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise
		law_sapience_mechanim_unrecognized
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods								
			goods_input_automata_add = 7
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -500
			building_employment_farmers_add = -200
		}
	}
}

pm_automata_harvesters_enforced = {
	texture = "gfx/interface/icons/production_method_icons/steam_powered_threshers.dds"
	unlocking_technologies = {
		early_mechanim		
	}
	
	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_automata_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_autonomous_tractors_enforced = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"

	unlocking_technologies = {
		advanced_mechanim			
	}

	unlocking_laws = {
		law_sapience_mechanim_compromise_enforced
		law_sapience_mechanim_unrecognized_enforced
	}
	is_hidden_when_unavailable = yes

	building_modifiers = {
		workforce_scaled = {
			# input goods								
			goods_input_automata_add = 8
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -500
			building_employment_farmers_add = -200
		}
	}
}

## Livestock
pm_geas_device = {
	texture = "gfx/interface/icons/production_method_icons/electric_fencing.dds"	
	unlocking_technologies = {
		psionic_theory	#its not in production but out there enough that its ok. as mentioned barbed wire is unlocked in military
	}
	building_modifiers = {
		workforce_scaled = {
			goods_input_artificery_doodads_add = 5
			goods_input_electricity_add = 4
		}

		level_scaled = {
			building_employment_laborers_add = -1600
			building_employment_machinists_add = -900
		}
	}

	required_input_goods = electricity
}